export default function RoutesMenu() {
  return {
    rutes: [
      {
        icon: 'mdi-ticket',
        active: false,
        items: false,
        title: 'tablero',
        to: '/'
      },
      {
        icon: 'mdi-silverware-fork-knife',
        active: false,
        items: false,
        title: 'Clientes',
        to: '/client'
      },
      {
        icon: 'mdi-school',
        active: false,
        items: [
          { title: 'Solicitud', to: '/request' },
          { title: 'Propuestas', to: '/proposals' },
          { title: 'Presupuestos', to: '/budgets' },
          { title: 'Documentos de ventas', to: '/salesDocuments' },
          { title: 'Tasa', to: '/rate' },
          { title: 'Pagos', to: '/sales' },
          { title: 'Notas de credito', to: '/creditNotes' },
          { title: 'Articulos', to: '/articles' },
        ],
        title: 'Ventas',
      },
      {
        icon: 'mdi-silverware-fork-knife',
        active: false,
        items: false,
        title: 'Suscripciones',
        to: '/subscriptions'
      },
      {
        icon: 'mdi-silverware-fork-knife',
        active: false,
        items: false,
        title: 'Control de visitas',
        to: '/visitControl'
      },
      {
        icon: 'mdi-silverware-fork-knife',
        active: false,
        items: false,
        title: 'Gastos',
        to: '/sales'
      },
      {
        icon: 'mdi-silverware-fork-knife',
        active: false,
        items: false,
        title: 'Contactos',
        to: '/contracts'
      },
      {
        icon: 'mdi-silverware-fork-knife',
        active: false,
        items: false,
        title: 'Proyectos',
        to: '/poryect'
      },
      {
        icon: 'mdi-silverware-fork-knife',
        active: false,
        items: false,
        title: 'Tareas',
        to: '/task'
      },
      {
        icon: 'mdi-silverware-fork-knife',
        active: false,
        items: [
          { title: 'Terminales', to: '/request' },
          { title: 'Sind', to: '/proposals' },
        ],
        title: 'Terminales'
      },
      {
        icon: 'mdi-ticket-account',
        active: false,
        items: false,
        title: 'Soporte',
        to: '/tickets'
      },
      {
        icon: 'mdi-silverware-fork-knife',
        active: false,
        items: false,
        title: 'Cliente potencial',
        to: '/potentialClient'
      },
      {
        icon: 'mdi-silverware-fork-knife',
        active: false,
        items: false,
        title: 'Bases de conocimientos',
        to: '/knowledgeBases'
      },
      {
        icon: 'mdi-silverware-fork-knife',
        active: false,
        items: false,
        title: 'Utilidades',
        to: '/Utilities'
      },
      {
        icon: 'mdi-silverware-fork-knife',
        active: false,
        items: false,
        title: 'Informes',
        to: '/Reports'
      },
      {
        icon: 'mdi-silverware-fork-knife',
        active: false,
        items: false,
        title: 'Imventario',
        to: '/Inventory'
      },
      {
        icon: 'mdi-silverware-fork-knife',
        active: false,
        items: false,
        title: 'Configuraciones',
        to: '/Settings'
      },
    ],
  }
}
